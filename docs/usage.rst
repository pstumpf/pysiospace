=====
Usage
=====

To use PysioSpace in a project::

    import pysiospace
    
See notebooks/PhysiospaceExample.ipynb for a short tutorial.
