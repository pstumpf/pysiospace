"""Top-level package for PysioSpace."""

__author__ = """Patrick Stumpf"""
__email__ = 'stumpf@combine.rwth-aachen.de'
__version__ = '0.1.0'

from .pp import run
from .plt import viz