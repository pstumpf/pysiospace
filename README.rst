==========
PysioSpace
==========


.. image:: https://img.shields.io/pypi/v/pysiospace.svg
        :target: https://pypi.python.org/pypi/pysiospace

.. image:: https://img.shields.io/travis/passt/pysiospace.svg
        :target: https://travis-ci.com/passt/pysiospace

.. image:: https://readthedocs.org/projects/pysiospace/badge/?version=latest
        :target: https://pysiospace.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




PhysioSpace methods for python


* Free software: MIT license
* Documentation: https://pysiospace.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
